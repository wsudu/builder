var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');

var cache = {
    index: 'lastVersion',
    createJson: function (dataObject) {
        return JSON.stringify({
            js: global.cfg.js,
            json: global.cfg.json,
            data: dataObject
        });
    },
    save: function (dataSave) {
        localStorage.setItem(this.index, this.createJson(dataSave));
    },
    isChange: function (dataActual) {
        var dataLast = localStorage.getItem(this.index);
        var dataActual = this.createJson(dataActual);
        
        if (dataLast == dataActual) {
            return false;
        } else {
            return true;
        }
    }
};

module.exports = cache;
