var fs = require('fs');

var parse = {
    meta: function (value) {
        var arrayReturn = {};
        var value = value.replace(/^(?=\n)$|^\s*|\s*$|\n\n+/gm, "").replace(/[\r\n]+[-]+[ ]/g, '>>').split(/\r?\n/);

        value.forEach(function (value) {
            var value = value.split(':');

            if (value[1].search('>>') >= 0) {
                value[1] = value[1].split('>>').filter(function (e) {
                    return e.trim().replace(/(\r\n|\n|\r)/gm, "")
                });
            } else {
                value[1] = value[1].trim();
            }

            arrayReturn[value[0]] = value[1];
        });

        return arrayReturn;
    },

    description: function (value) {
        var arrayReturn = {
            description: ''
        };
        var descriptionArray = (value.trim().split('*/')[0]).split('```');

        if (descriptionArray[0] !== undefined && descriptionArray[0].trim().length) {
            arrayReturn.description = descriptionArray[0].trim();
        }

        return arrayReturn;
    },

    example: function (value) {
        var arrayReturn = {
            type: '',
            code: ''
        };
        var exampleArray = (value.trim().split('*/')[0]).split('```');

        if (exampleArray[1] !== undefined && exampleArray[1].trim().length) {
            exampleArray = exampleArray[1].trim().split(/\r?\n/);

            arrayReturn.type = exampleArray[0].trim();
            exampleArray.shift();
            arrayReturn.code = exampleArray.join('\r\n').trim();
        }

        return arrayReturn;
    },

    css: function (value) {
        var arrayReturn = {
            css: ''
        };
        var cssArray = value.trim().replace('*/', '@#!?').split('@#!?');

        if (cssArray[1] !== undefined && cssArray[1].trim().length) {
            arrayReturn.css = (cssArray[1].slice(0, -2)).trim();
        }

        return arrayReturn;
    },
}

var parseComponents = function (cssString) {
    var dataList = {};

    var folderTest = function () {
        if (!fs.existsSync(global.cfg.folder)) {
            fs.mkdirSync(global.cfg.folder);
        }
    }();

    function getSlug(meta, index) {

        return (index + '-' + meta.name)
            .toLowerCase()
            .replace(/\s+/g, "-")
            .replace(/[^\w\-]+/g, "-")
            .replace(/\-\-+/g, "-")
            .replace(/^-+/, "")
            .replace(/-+$/, "");
    };

    var masterParse = function (cssString) {
        var newItem = [];
        var newDetail = [];
        var cssArray = cssString.split("---");
        var componentIndex = 1;

        cssArray.forEach(function (value, index) {
            var value = value.trim();

            // parse meta
            if (value.search('name:') >= 0 && value.search('category:') >= 0) {
                var newItem = parse.meta(value);
                var slug = getSlug(newItem, componentIndex);

                // parse detail
                if (cssArray[index + 1].search('name:') < 0 && cssArray[index + 1].search('category:') < 0) {

                    if (cssArray[index + 1].slice(-2) == '/*') {
                        cssArray[index + 1] = cssArray[index + 1].substring(0, cssArray[index + 1].length - 2);
                    }

                    newItem = Object.assign({},
                        newItem,
                        parse.description(cssArray[index + 1]),
                        parse.example(cssArray[index + 1])
                    );
                }

                dataList[slug] = newItem;
                componentIndex++;
            }
        });
    }(cssString);

    return dataList;
};

module.exports = parseComponents;
