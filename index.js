var fs = require('fs');
var parseComponents = require('./lib/parse.js');
var cache = require('./lib/cache.js');

var cfg = {
    folder: '.',
    json: true,
    js: false,
    beautify: false,
    callback: new Function()
};

var saved = {};

var saveToFile = function (data, pathFile, callBack) {
    fs.writeFile(pathFile, data, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log('WSUDU: Save ' + pathFile);
        saved[pathFile] = true;
        callBack();
    });
};

var saveData = function (dataArray) {
    var callBack = function () {
        if ((global.cfg.json && !global.cfg.js && saved[jsonPath]) ||
            (!global.cfg.json && global.cfg.js && saved[jsPath]) ||
            (global.cfg.json && global.cfg.js && saved[jsPath] && saved[jsonPath])) {
            global.cfg.callback();
        }
    };

    var jsonPath = global.cfg.folder + '/wsudu.json';
    var jsPath = global.cfg.folder + '/wsudu.js';
    var jsonData = JSON.stringify(dataArray, null, global.cfg.beautify ? 2 : 0);

    if (!cache.isChange(jsonData)) {
        if (!((global.cfg.json && !fs.existsSync(jsonPath)) ||
                (global.cfg.js && !fs.existsSync(jsPath))
            )) {
            console.log('WSUDU: No change in documentation');
            global.cfg.callback();
            return false;
        }
    }

    if (global.cfg.json) {
        saveToFile(jsonData, jsonPath, callBack);
    }

    if (global.cfg.js) {
        var jsData = 'var wsuduData = ' + jsonData + ';';
        saveToFile(jsData, jsPath, callBack);
    }

    cache.save(jsonData);
}

var consoleRed = function (message) {
    console.log('\x1b[41m%s\x1b[0m', 'WSUDU: ' + message);
};


var parseCssString = function (cssString, userCfg, callback) {
    global.cfg = Object.assign({}, cfg, userCfg);

    if (!global.cfg.json && !global.cfg.js) {
        consoleRed('Is not set output format (js or json)');
        return false;
    }

    var dataArray = parseComponents(cssString);
    saveData(dataArray);
}

module.exports = parseCssString;
